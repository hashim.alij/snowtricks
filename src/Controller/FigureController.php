<?php

namespace App\Controller;

use App\Entity\Commentaire;
use App\Entity\Figure;
use App\Entity\Illustration;
use App\Form\CommentaireType;
use App\Form\FigureType;
use App\Repository\CommentaireRepository;
use DateTimeImmutable;
use DateTimeZone;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Response;

class FigureController extends AbstractController
{

    #[Route('/figures/detail/{slug}', name: 'figure_detail')]
    public function detail(Figure $figure, CommentaireRepository $commentaireRepository, Request $request, EntityManagerInterface $em,): Response
    {
        $current_page = $request->get('page') ? $request->get('page') : 1;
        $del_ill = $request->get('del_ill') ? $request->get('del_ill') : false;
        $commentaires = $commentaireRepository->paginateCommentaires($request, $figure, $current_page);
        $commentaire = new Commentaire();
        $commentaire->setUser($figure->getUser());
        $commentaire->setFigure($figure);
        $date = new DateTimeImmutable("now", new DateTimeZone('Europe/Paris'));
        $commentaire->setDateCreation($date);
        $form = $this->createForm(CommentaireType::class, $commentaire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($commentaire);
            $em->flush();
            $this->addFlash("success", "Votre commentaire a bien été ajouté");
            return $this->redirectToRoute('figure_detail', ['slug' => $figure->getSlug()]);
        } else {
            return $this->render('figure/presentation.html.twig', ['figure' => $figure, "form" => $form, "commentaires" => $commentaires, "page" => $current_page, $del_ill => "del_ill"]);
        }
    }

    #[Route('/ajouter', name: 'figure_ajouter')]
    public function ajouter(Request $request, EntityManagerInterface $em,): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        $figure = new Figure();
        $figure->setUser($user);
        $date = new DateTimeImmutable("now", new DateTimeZone('Europe/Paris'));
        $figure->setDateCreation($date);

        $form = $this->createForm(FigureType::class, $figure);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $figure->setSlug((new Slugify())->slugify($figure->getNom()));
            $em->persist($figure);
            $em->flush();
            $this->addFlash("success", "Votre figure a bien été créée");
            return $this->redirectToRoute('figure_ajouter');
        }
        return $this->render('figure/ajouter.html.twig', ['form' => $form]);
    }

    #[Route('/figure/{slug}/modifier', name: 'figure_modifier')]
    public function modifier(Request $request, EntityManagerInterface $em, Figure $figure): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $form = $this->createForm(FigureType::class, $figure);
        $form->handleRequest($request);
        $date = new DateTimeImmutable("now", new DateTimeZone('Europe/Paris'));
        $figure->setDateModification($date);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            $this->addFlash("success", "Votre figure a bien été modifiée");
            return $this->redirectToRoute('figure_modifier', ['slug' => $figure->getSlug()]);
        }
        return $this->render('figure/modifier.html.twig', ['form' => $form->createView()]);
    }

    #[Route('/figure/{slug}/delete', name: 'figure_delete')]
    public function delete(EntityManagerInterface $em, Figure $figure): Response
    {

        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $em->remove($figure);
        $em->flush();
        $this->addFlash("success", "La figure a bien été suprimée");
        return $this->redirectToRoute('accueil');
    }

    #[Route('/illustration/{id}/delete', name: 'illustration_delete')]
    public function illustrationDelete(Illustration $illustration, EntityManagerInterface $em,): Response
    {

        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $em->remove($illustration);
        $em->flush();
        $this->addFlash("success", "La figure a bien été suprimée");
        return $this->redirectToRoute('figure_detail', ['id' => $illustration->getFigure()->getId()] );
    }
}
