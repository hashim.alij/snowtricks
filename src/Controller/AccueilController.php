<?php

namespace App\Controller;

use App\Repository\FigureRepository;
use App\Repository\UserRepository;
use App\Repository\UtilisateurRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class AccueilController extends AbstractController
{

    #[Route('/', name: 'accueil')]
    public function index(FigureRepository $figureRepository, Request $request): Response
    {

        $connection = true;
        $del_slug = $request->get('del_slug') ? $request->get('del_slug') : false;
        $current_page = $request->get('page') ? $request->get('page') : 1;
        $figures = $figureRepository->paginateFigures($request, $current_page);
        return $this->render('index.html.twig', ["connection" => $connection, "figures" => $figures, "page" => $current_page, "del_slug" => $del_slug]);
    }
}
