<?php
// src/EventListener/FigureListener.php
namespace App\EventListener;

use App\Entity\Figure;
use App\Repository\FigureRepository;
use Symfony\Component\String\Slugger\SluggerInterface;

class FigureEventListener
{
        /**
     * @var FigureRepository
     */
    private $figureRepository;

    public function __construct(FigureRepository $figureRepository, private SluggerInterface $slugger,)
    {
        $this->figureRepository = $figureRepository;
    }

    public function prePersist(Figure $figure)
    {
        $figure->setSlug($this->slugger->slug($figure->getNom()));
        
    }
}