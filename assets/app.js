// import "./bootstrap.js";
import "./styles/app.css";

console.log("This log comes from assets/app.js - welcome to AssetMapper! 🎉");

document.querySelectorAll(".add_ill_link").forEach((btn) => {
  btn.addEventListener("click", addIllustrationFormToCollection);
});

document.querySelectorAll("ul.illustrations li").forEach((illustration) => {
  addIllustrationFormDeleteLink(illustration);
});

function addIllustrationFormToCollection(e) {
  const collectionHolder = document.querySelector(
    "." + e.currentTarget.dataset.collectionHolderClass
  );

  const item = document.createElement("li");

  item.innerHTML = collectionHolder.dataset.prototype.replace(
    /__name__/g,
    collectionHolder.dataset.index
  );

  collectionHolder.appendChild(item);

  collectionHolder.dataset.index++;
  // add a delete link to the new form
  addIllustrationFormDeleteLink(item);
}

function addIllustrationFormDeleteLink(item) {
  const removeFormButton = document.createElement("button");
  removeFormButton.innerText = "Supprimer";
  removeFormButton.className += " btn btn-danger";

  item.append(removeFormButton);

  removeFormButton.addEventListener("click", (e) => {
    e.preventDefault();
    // remove the li for the Illustration form
    item.remove();
  });
}

///// Videos

document.querySelectorAll(".add_vid_link").forEach((btn) => {
  btn.addEventListener("click", addVideoFormToCollection);
});

document.querySelectorAll("ul.videos li").forEach((video) => {
  addVideoFormDeleteLink(video);
});

function addVideoFormToCollection(e) {
  const collectionHolder = document.querySelector(
    "." + e.currentTarget.dataset.collectionHolderClass
  );

  const item = document.createElement("li");

  item.innerHTML = collectionHolder.dataset.prototype.replace(
    /__name__/g,
    collectionHolder.dataset.index
  );

  collectionHolder.appendChild(item);

  collectionHolder.dataset.index++;
  // add a delete link to the new form
  addVideoFormDeleteLink(item);
}

function addVideoFormDeleteLink(item) {
  const removeFormButton = document.createElement("button");
  removeFormButton.innerText = "Supprimer";
  removeFormButton.className += " btn btn-danger";

  item.append(removeFormButton);

  removeFormButton.addEventListener("click", (e) => {
    e.preventDefault();
    // remove the li for the Video form
    item.remove();
  });
}

let medias = document.querySelector("#medias");
function showMedias() {
  console.log("alive !!!");
  if (medias.style.display == "none") {
    medias.style.display = "block";
  } else {
    medias.style.display = "none";
  }
}

document.querySelectorAll("#mediaShow").forEach((btn) => {
  btn.addEventListener("click", showMedias);
});
