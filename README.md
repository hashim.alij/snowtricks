# SnowTricks

## Description

Ce projet est basé sur la creation d'un site collaboratif pour faire connaître le snowboard auprès du grand public et aider à l'apprentissage des figures (tricks).

## Getting started

Le framework symfony est utilisé pour la creation de ce projet

## Prérequis

- PHP 8.0.8 ou supérieur
- Composer
- Symfony CLI

## Installation

Suivez ces étapes pour démarrer le projet :

**Cloner le dépôt**

```sh
git clone https://gitlab.com/hashim.alij/snowtricks.git
cd SnowTricks
```

## Installer les dépendances :

```sh
composer install
```

**Configurer les variables d'environnement :\***

Créer un fichier `.env` et configurez-le selon votre environnement.

Mettez à jour l'URL de la base de données et d'autres paramètres spécifiques à l'environnement dans `.env.local`.

Créer la base de données :

```sh
php bin/console doctrine:database:create
php bin/console doctrine:schema:update --force
```

## Lancer le serveur local Symfony :

```sh
symfony server:start
```

L'application sera disponible à l'adresse `https://http://127.0.0.1:8000/`.

## Utilisation

Pour accéder à l'application, ouvrez votre navigateur et allez à `https://http://127.0.0.1:8000/`.

Exécuter les migrations de base de données :

```sh
php bin/console doctrine:migrations:migrate
```
